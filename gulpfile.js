var gulp = require('gulp'),
    sass = require('gulp-sass'),
    less = require('gulp-less'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    minifyCSS = require('gulp-minify-css'),
    cleanCSS = require('gulp-clean-css'),
    webserver = require('gulp-webserver');

var paths = {
    'vendor': 'node_modules/',
    'app': 'app/',
    'assets' : 'assets/'
};

gulp.task('vendor:scripts', function () {
    return gulp.src([
        paths.vendor + 'angular/angular.min.js',
        paths.vendor + 'angular-animate/angular-animate.js',
        paths.vendor + 'jquery/dist/jquery.min.js'
    ])
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(gulp.dest('scripts'))
});

gulp.task('vendor:styles', function () {
    return gulp.src(paths.assets + 'less/bootstrap.less')
        .pipe(less())
        .pipe(minifyCSS())
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'))
});

gulp.task('styles', function () {
    return gulp.src(paths.assets + 'scss/*.scss')
        .pipe(sass({style: 'compressed'}).on('error', sass.logError))
        .pipe(concat('styles.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('css'))
});

gulp.task('scripts', function () {
    return gulp.src([
        paths.app + 'main.js',
        paths.app + '**/*.module.js',
        paths.app + '**/*.model.js',
        paths.app + '**/*.service.js',
        paths.app + '**/*.factory.js',
        paths.app + '**/*.filter.js',
        paths.app + '**/*.directive.js',
        paths.app + '**/*.component.js',
        paths.app + '**/*.util.js',
        paths.app + '**/*.controller.js',
        paths.app + '**/*.routes.js',
        paths.app + '**/*.interceptor.js'
    ])
        .pipe(concat('app.js'))
        .pipe(jshint(jshint_config))
        .pipe(jshint('.jshintrc'))
        .pipe(uglify())
        .pipe(gulp.dest('scripts'))
});

gulp.task('vendors', ['vendor:scripts', 'vendor:styles']);

gulp.task('run', function () {
    gulp.start('vendors', 'scripts', 'styles', 'watch', 'webserver');
});

gulp.task('webserver', function () {
    gulp.src('./')
        .pipe(webserver({
            port: 8080,
            livereload: true,
            directoryListing: false,
            open: true
        }));
});

gulp.task('watch', function () {
    gulp.watch(paths.app + '**/*.html');
    gulp.watch(paths.app + '**/*.js', ['scripts']);
    gulp.watch(paths.assets + 'scss/**/*.scss', ['styles']);
});

var jshint_config = {
    "bitwise": true,
    "camelcase": true,
    "curly": true,
    "eqeqeq": true,
    "es3": false,
    "forin": true,
    "freeze": true,
    "immed": true,
    "indent": 4,
    "latedef": "nofunc",
    "newcap": true,
    "noarg": true,
    "noempty": true,
    "nonbsp": true,
    "nonew": true,
    "plusplus": false,
    "quotmark": "single",
    "undef": true,
    "unused": false,
    "strict": false,
    "maxparams": 10,
    "maxdepth": 5,
    "maxstatements": 40,
    "maxcomplexity": 8,
    "maxlen": 120,

    "asi": false,
    "boss": false,
    "debug": false,
    "eqnull": true,
    "esnext": false,
    "evil": false,
    "expr": false,
    "funcscope": false,
    "globalstrict": false,
    "iterator": false,
    "lastsemic": false,
    "laxbreak": false,
    "laxcomma": false,
    "loopfunc": true,
    "maxerr": false,
    "moz": false,
    "multistr": false,
    "notypeof": false,
    "proto": false,
    "scripturl": false,
    "shadow": false,
    "sub": true,
    "supernew": false,
    "validthis": false,
    "noyield": false,

    "browser": true,
    "node": true,

    "globals": {
        "angular": false,
        "$": false
    }
};


