angular
    .module('app.game')
    .controller('CounterController', CounterController);

CounterController.$inject = ['$scope'];
function CounterController($scope) {
    var vm = this;

    vm.rangeHelper = rangeHelper;

    /////////////////////////////////////////////////////////////////

    function rangeHelper(n) {
        var array = [];
        for (var i = 0; i < n; i++) {
            array.push(i);
        }
        return array;
    }

    $scope.$watch(vm.attempts, function (newValue, oldValue) {
        console.log(newValue);
    });

}