angular
    .module('app.game')
    .controller('GameController', GameController);

GameController.$inject = ['GameService', 'gameStatus', 'tileStatus', 'gameConfig'];
function GameController(GameService, gameStatus, tileStatus, gameConfig) {
    var vm = this;
    var gameSize = gameConfig.board_size;

    vm.attempts = GameService.getAttempts();
    vm.max_attempts = gameConfig.max_attempts;

    startGame();
    observeGrid();
    observeResult();

    /////////////////////////////////////////////////////////////////

    vm.getGameSize = function () {
        return new Array(gameSize);
    };

    function startGame() {
        GameService.generateGrid()
            .then(function() {
                vm.initiated = true;
                console.log(vm.initiated);
            });
    }

    function observeGrid() {
        GameService.observeGrid()
            .then(null, null, function () {
                vm.grid = GameService.getGrid();
                vm.attempts = GameService.getAttempts();
            });
    }

    function observeResult() {
        GameService.observeResult()
            .then(null, null, function (result) {
                vm.result = result;
                if (result == gameStatus.lose) {
                    GameService.complete();
                }
            });
    }

}