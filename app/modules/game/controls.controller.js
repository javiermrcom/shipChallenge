angular
    .module('app.game')
    .controller('ControlsController', ControlsController);

ControlsController.$inject = ['GameService'];
function ControlsController(GameService) {
    var vm = this;

    vm.cheat = cheat;
    vm.restart = restart;
    vm.cheated = false;

    var gridCached = [];

    /////////////////////////////////////////////////////////////////

    function cheat() {
        if(!vm.cheated) {
            gridCached = angular.copy(GameService.getGrid());
            GameService.complete();
        }
        else {
            GameService.uncomplete();
            GameService.updateGrid(gridCached);
        }

        toggleCheat();
    }

    function restart() {
        GameService.generateGrid();
    }

    function toggleCheat(value) {
        vm.cheated = value ? value : !vm.cheated;
    }

}