angular
    .module('app.game')
    .component('tile', {
        templateUrl: '/app/modules/game/tile.tpl.html',
        controller: TileController,
        controllerAs: 'vm',
        bindings: {
            key: '=',
            attempts: '='
        }
    });