angular
    .module('app.game', [])
    .constant('gameStatus', {
        'lose': 'lose',
        'win': 'win'
    })
    .constant('tileStatus', {
        'back': 'back',
        'water': 'water',
        'boat': 'boat'
    })
    .constant('gameConfig', {
        'board_size': 8,
        'tiles': 64,
        'max_attempts': parseInt(64/3),
        'boat_tiles': parseInt(3 + 4 + 5),
        'attempts_win': parseInt(64 - 3 - 4 - 5)
    });