angular
    .module('app.game')
    .component('controls', {
        templateUrl: '/app/modules/game/controls.tpl.html',
        controller: ControlsController,
        controllerAs: 'vm',
        bindings: {
            result: '@'
        }
    });