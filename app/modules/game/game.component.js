angular
    .module('app.game')
    .component('game', {
        templateUrl: '/app/modules/game/game.tpl.html',
        controller: GameController,
        controllerAs: 'vm',
        bindings: {
            name: '='
        }
    });