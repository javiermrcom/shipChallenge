angular
    .module('app.game')
    .component('counter', {
        templateUrl: '/app/modules/game/counter.tpl.html',
        controller: CounterController,
        controllerAs: 'vm',
        bindings: {
            attempts: '@',
            result: '@',
            restAttempts: '@'
        }
    });