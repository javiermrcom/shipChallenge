angular
    .module('app.game')
    .controller('TileController', TileController);

TileController.$inject = ['$scope', 'GameService'];
function TileController($scope, GameService) {
    var vm = this;

    vm.click = click;

    /////////////////////////////////////////////////////////////////

    function click(tile) {
        GameService.clickTile(tile);
        vm.attempts = GameService.getAttempts();
    }

}