angular
    .module('app.game')
    .factory('GameService', GameService);

GameService.$inject = ['$q', 'gameStatus', 'tileStatus', 'gameConfig'];
function GameService($q, gameStatus, tileStatus, gameConfig) {

    var vm = this,
        deferGrid = $q.defer(),
        deferResult = $q.defer();

    var attempts;
    var successAttempts;

    var service = {
        getAttempts: getAttempts,
        getGrid: getGrid,
        setGrid: setGrid,
        generateGrid: generateGrid,
        clickTile: clickTile,
        complete: complete,
        uncomplete: uncomplete,
        observeGrid: observeGrid,
        observeResult: observeResult,
        updateGrid: updateGrid
    };

    return service;

    /////////////////////////////////////////

    function observeGrid() {
        return deferGrid.promise;
    }

    function observeResult() {
        return deferResult.promise;
    }

    function getGrid() {
        return vm.grid;
    }

    function getAttempts() {
        return attempts;
    }

    function clickTile(key) {
        if (!vm.result)
            angular.forEach(vm.grid, function (row, rowKey) {
                angular.forEach(row, function (tile, tileKey) {
                    if (tile.key == key) {
                        if (vm.grid[rowKey][tileKey].status == tileStatus.back) {
                            attempts++;
                            if (vm.grid[rowKey][tileKey].boat) {
                                vm.grid[rowKey][tileKey].status = tileStatus.boat;
                                successAttempts++;
                                if (successAttempts == gameConfig.boat_tiles) {
                                    setResult(gameStatus.win);
                                    complete();
                                }
                            } else {
                                vm.grid[rowKey][tileKey].status = tileStatus.water;
                                if (attempts >= gameConfig.max_attempts) {
                                    setResult(gameStatus.lose);
                                    complete();
                                }
                            }
                        }
                    }
                });
            }, vm.grid);

        setGrid(vm.grid);
    }

    function generateGrid() {
        var defered = $q.defer();
        var promise = defered.promise;
        var newGrid = [];

        resetGrid();

        var boatSizes = [3, 4, 5];
        attempts = 0;
        successAttempts = 0;

        for (var i = 0; i < gameConfig.board_size; i++) {
            newGrid[i] = [];
            for (var j = 0; j < gameConfig.board_size; j++) {
                newGrid[i][j] = {
                    'key': '' + parseInt(i) + ',' + parseInt(j) + '',
                    'status': 'back',
                    'boat': false
                };
            }
        }

        setGrid(newGrid);

        boatSizes.reduce(function (p, val) {
            return p.then(function () {
                return generateBoat(val);
            });
        }, $q.when(true)).then(function () {
            defered.resolve(true);
        }, function (err) {
        });

        return promise;
    }

    function generateBoat(boatSize) {
        var defered = $q.defer();
        var promise = defered.promise;

        var newGrid;
        var dir = Math.random() > 0.5 ? 'H' : 'V';
        var x, y, i, isIn;
        var valid = false;

        setResult();

        while (!valid) {
            newGrid = getGrid();
            if (dir == 'V') {
                x = Math.floor((Math.random() * (gameConfig.board_size - 1)) + 1);
                y = Math.floor((Math.random() * parseInt(gameConfig.board_size - parseInt(boatSize + 1))) + 1);
                valid = true;

                for (i = 0; i < boatSize; i++) {
                    isIn = newGrid[parseInt(y + i)][x].boat;
                    if (isIn) {
                        valid = false;
                        break;
                    }
                }
                if (valid)
                    for (i = 0; i < boatSize; i++) {
                        newGrid[parseInt(y + i)][x].boat = getBoatClass(boatSize);
                    }

            } else {
                x = Math.floor((Math.random() * parseInt(gameConfig.board_size - parseInt(boatSize + 1))) + 1);
                y = Math.floor((Math.random() * (gameConfig.board_size - 1)) + 1);
                valid = true;

                for (i = 0; i < boatSize; i++) {
                    isIn = newGrid[y][parseInt(x + i)].boat;
                    if (isIn) {
                        valid = false;
                        break;
                    }
                }
                if (valid)
                    for (i = 0; i < boatSize; i++) {
                        newGrid[y][parseInt(x + i)].boat = getBoatClass(boatSize);
                    }
            }
        }

        setGrid(newGrid);
        defered.resolve(newGrid);
        return promise;
    }

    function complete() {
        angular.forEach(vm.grid, function (row, rowKey) {
            angular.forEach(row, function (tile, tileKey) {
                vm.grid[rowKey][tileKey].status = vm.grid[rowKey][tileKey].boat ? tileStatus.boat : tileStatus.water;
            });
        }, vm.grid);
    }

    function uncomplete() {
        angular.forEach(vm.grid, function (row, rowKey) {
            angular.forEach(row, function (tile, tileKey) {
                vm.grid[rowKey][tileKey].status = 'back';
            });
        }, vm.grid);
    }

    function resetGrid() {
        vm.grid = [];
    }

    function setGrid(newGrid) {
        vm.grid = newGrid;
        deferGrid.notify(vm.grid);
    }

    function updateGrid(newGrid) {
        angular.forEach(vm.grid, function (row, rowKey) {
            angular.forEach(row, function (tile, tileKey) {
                vm.grid[rowKey][tileKey] = newGrid[rowKey][tileKey];
            });
        }, vm.grid);

        deferGrid.notify(vm.grid);
    }

    function setResult(result) {
        vm.result = result;
        deferResult.notify(vm.result);
    }

    function getBoatClass(boatSize) {
        return 'boat-' + boatSize;
    }

}