angular
    .module('app.game')
    .component('gameTitle', {
        templateUrl: '/app/modules/game/gameTitle.tpl.html',
        controller: GameTitleController,
        controllerAs: 'vm',
        bindings: {
            title: '@'
        }
    });