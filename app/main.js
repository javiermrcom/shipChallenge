angular
    .module('app', [
        'ngAnimate',
        // MODULES
        'app',
        'app.game'
    ])
    .config(appConfig)
    .run(appRun)
    .value('$routerRootComponent', 'app');

appConfig.$inject = [
    '$httpProvider'
];

function appConfig($httpProvider) {
}

appRun.$inject = ['$rootScope'];
function appRun($rootScope) {
}